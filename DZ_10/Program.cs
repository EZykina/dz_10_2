﻿using System;

namespace DZ_4_Z3
{
    class Program
    {
        static void Main(string[] args)
        {/*Создайте константу с именем -pi (число π «пи»), создайте переменную радиус с именем – r. 
           Используя формулу πR2, вычислите площадь круга и выведите результат на экран.*/

            const double pi = 3.14;

            Console.WriteLine("Введите радиус круга");
            int r = int.Parse(Console.ReadLine());
            double S = Math.Round(pi * Math.Pow(r, 2), 2);

            Console.WriteLine("Площадь SS круга радиусом {0} равна {1}", r, S);
            Console.ReadKey();

        }
    }
}
